/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.tablas;

import controlador.cola.ColaServices;
import java.text.SimpleDateFormat;
import javax.swing.table.AbstractTableModel;
import modelo.Historial;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TablaComandos extends AbstractTableModel{
    ColaServices<Historial> hc;

    public TablaComandos(ColaServices<Historial> hc) {
        this.hc = hc;
    }
    
    @Override
    public int getRowCount() {
       return hc.getSize();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Fecha Creacion";
            case 1:
                return "Comando";
            default:
                return null;
        }            
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Historial r =  new Historial();
        r = hc.obtenerDato(rowIndex);
        switch (columnIndex) {
            case 0:
                return new SimpleDateFormat("yyyy/MM/dd , hh:mm:ss").format(r.getFecha());
            case 1:
                return r.getComando();
            default:
                return null;
        }
    }
    
}
