/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import controlador.cola.ColaServices;
import modelo.Historial;


/**
 *
 * @author LENOVO LEGION 5
 */
public class HistorialController {
    ColaServices<Historial> listaHistorial = new ColaServices<>(10);

    public HistorialController() {
    }

    public ColaServices<Historial> getListaHistorial() {
        return listaHistorial;
    }

    public void setListaHistorial(ColaServices<Historial> listaHistorial) {
        this.listaHistorial = listaHistorial;
    }
    
    public int getSize(){
        return listaHistorial.getSize();
    }
}
