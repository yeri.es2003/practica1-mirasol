/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.cola;

import controlador.lista.ListaEnlazada;
import controlador.exception.EstructuraDataVaciaExpetion;
import controlador.exception.PosicionException;
import controlador.exception.TopeException;

/**
 *
 * @author sebastian
 */
public class Cola <E> extends ListaEnlazada<E> {
    private Integer tope;

    public Cola(Integer tope) {
        this.tope = tope;
    }
    
    public Boolean estaLleno() {
        if(tope == 0)
            return false;
        else if(tope == getSize().intValue())
            return true;
        else
            return false;
    }
    
    public void push(E dato) throws TopeException, PosicionException, EstructuraDataVaciaExpetion {
        if(!estaLleno()) {
            insertarCabecera(dato);
        } else{
            throw new PosicionException("La cola esta llena");
        }
    }
    
    public E pop(Integer pos) throws EstructuraDataVaciaExpetion, PosicionException {
        E auxdato =  null;
        if(!estaVacia()) {
            if (pos >= 0 && pos < getSize()) {
                for(int i = tope-1; i >= pos; i--) {
                    auxdato = eliminarDato(i);
                }
                return auxdato;

            } else {
                throw new PosicionException("Error al realizar la operacion DEQUEUE No existe la posicion dada");
            }
        } else
            throw new EstructuraDataVaciaExpetion("La cola esta vacia");
    }

    public Integer getTope() {
        return tope;
    }
    
}
