/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package practica3.optimizacion;

import java.util.Scanner;

/**
 *
 * @author LENOVO LEGION 5
 */
public class Memoria {
    private static final int OVERFLOW = 200000000;
    static String salida;

    public Memoria() {
        salida = new String("a".repeat(OVERFLOW));
        salida = "";
    }
    
    public int getOom(){
        return salida.length();  
    }
    
    public static void main(String[] args) {
        Memoria javaHeapTest = new Memoria();
        System.out.println(javaHeapTest.getOom());
        Scanner sc = new Scanner(System.in);
        System.out.println("precione cualquier numero");
        sc.nextInt();
    }
}

