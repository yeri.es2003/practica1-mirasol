/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.util.ArrayList;
import java.util.List;
import org.github.jamm.MemoryMeter;

/**
 *
 * @author LENOVO LEGION 5
 */
public class Memoria {
    private long [] [] aux =  new long [16] [2];

    public Memoria() {
        MemoryMeter meter = MemoryMeter.builder().build();
        String mString = "";
        aux [0] [0] = meter.measureDeep(mString);
        mString = "Esto es un Ejemplo";
        aux [0] [1] = meter.measureDeep(mString);
        int mint = 0;
        aux [1] [0] = meter.measureDeep(mint);
        mint = 35;
        aux [1] [1] = meter.measureDeep(mint);
        float mfloat = 0.0f;
        aux [2] [0] = meter.measureDeep(mfloat);
        mfloat = (float) 485.1254;
        aux [2] [1] = meter.measureDeep(mfloat);
        double mdouble = 0.0d;
        aux [3] [0] = meter.measureDeep(mdouble);
        mdouble = (double) 485.1254;
        aux [3] [1] = meter.measureDeep(mdouble);
        boolean mboolean = false;
        aux [4] [0] = meter.measureDeep(mboolean);
        mboolean = true;
        aux [4] [1] = meter.measureDeep(mboolean);
        long mlong = 0l;
        aux [5] [0] = meter.measureDeep(mlong);
        mlong = 1534548953;
        aux [5] [1] = meter.measureDeep(mlong);
        short mshort  = 0;
        aux [6] [0] = meter.measureDeep(mshort);
        mshort = 1454;
        aux [6] [1] = meter.measureDeep(mshort);
        char mchar = '\u0000';
        aux [7] [0] = meter.measureDeep(mchar);
        mchar = 'x';
        aux [7] [1] = meter.measureDeep(mchar);
        byte mbyte = 0;
        aux [8] [0] = meter.measureDeep(mbyte);
        mbyte = 116;
        aux [8] [1] = meter.measureDeep(mbyte);
        String [] amString = new String [5];
        aux [9] [0] = meter.measureDeep(amString);
        amString = new String []{"Ejemplo","Hola","Mundo","Relleno","Relleno"};
        aux [9] [1] = meter.measureDeep(amString);
        int [] amint = {};
        aux [10] [0] = meter.measureDeep(amint);
        amint = new int [] {45,654,654,231,25,45,38489};
        aux [10] [1] = meter.measureDeep(amint);
        float [] amfloat = {};
        aux [11] [0] = meter.measureDeep(amfloat);
        amfloat = new float [] {351.55f,51351.2f,51.12145f,2121.46862f};
        aux [11] [1] = meter.measureDeep(amfloat);
        boolean [] amboolean = {};
        aux [12] [0] = meter.measureDeep(amboolean);
        amboolean = new boolean [] {true,true,true,true,true};
        aux [12] [1] = meter.measureDeep(amboolean);
        char [] amchar = {};
        aux [13] [0] = meter.measureDeep(amchar);
        amchar = new char [] {'a','c','d','e','g','x'};
        aux [13] [1] = meter.measureDeep(amchar);
        List<String> lmString = new ArrayList<String>();
        aux [14] [0] = meter.measureDeep(lmString);
        for (int i = 0; i < 25; i++) {
            lmString.add("Palabra " +i);
        }
        aux [14] [1] = meter.measureDeep(lmString);
        ListaEnlazada<String> lemString = new ListaEnlazada<>();
        aux [15] [0] = meter.measureDeep(lemString);
        for (int i = 0; i < 25; i++) {
            lemString.insertar("Palabra " +i);
        }
        aux [15] [1] = meter.measureDeep(lemString);
        
    }

    public long[][] getAux() {
        return aux;
    }
}

