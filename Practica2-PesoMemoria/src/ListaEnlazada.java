/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


import Exception.PosicionException;

/**
 *
 * @author LENOVO LEGION 5
 */
public class ListaEnlazada <E>{
    private NodoLista <E> cabecera;  
    private Integer size;
    
    /*
    * Contructor de la clase se inicializa en null la cabecera y en 0 el size;
    */
    
    public ListaEnlazada() {
        this.cabecera = null;
        size = 0;
    }
    
    /*
    * Permite ver si la lista esta vacia 
    * @return true si esta vacia, false si esta llena
    */
    
    public Boolean estaVacia(){
        return cabecera == null;
    }

    public Integer getSize() {
        return size;
    }
    
    public void insertar(E dato){
        NodoLista <E> nuevo = new NodoLista(dato, null);
        if(estaVacia()){
            cabecera = nuevo;
        }else{
            NodoLista <E> aux = cabecera;
            while(aux.getSiguiente() != null){
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(nuevo);
        }
        size++;
    }
    
    
    
    
}
