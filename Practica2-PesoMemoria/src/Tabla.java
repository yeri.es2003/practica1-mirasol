
import javax.swing.table.AbstractTableModel;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author LENOVO LEGION 5
 */
public class Tabla extends AbstractTableModel {
    long [] [] pesos;
    String [] tipos = {"String","Int","Float","Double","Boolean","Long","Short","Char","Byte","String []", "Int []", "Float []", "Boolean []", "Char []", "Lista String", "ListaEnlazada String"};
    
    public Tabla (long [] [] pesos){
        this.pesos = pesos;
    }
    
    @Override
    public int getRowCount() {
        return 16;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        long [] aux = pesos [rowIndex];
        switch (columnIndex) {
            case 0:
                return tipos[rowIndex];
            default:
                return aux[columnIndex -1];
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Tipo";
            case 1:
                return "default";
            case 2:
                return "no default";
            default:
                throw new AssertionError();
        }
    }
    
    
    
}
