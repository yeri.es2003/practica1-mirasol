/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Ciudad;

import controlador.tda.grafos.GrafoNDE;
import modelo.Ciudad;
import modelo.Ubicacion;

/**
 *
 * @author LENOVO LEGION 5
 */
public class CiudadGrafoController {
    private GrafoNDE<Ciudad> gnde;
    private Ciudad ciudad;

    public CiudadGrafoController() {
        gnde=new GrafoNDE<>(0,Ciudad.class);
    }

    public GrafoNDE<Ciudad> getGnde() {
        return gnde;
    }

    public void setGnde(GrafoNDE<Ciudad> gend) {
        this.gnde = gend;
    }

    public Ciudad getCiudad() {
        if(ciudad==null){
            ciudad= new Ciudad();
        }
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }
    
    public Double calcularDistancia(Ciudad co, Ciudad cd) {
        // raiz cuadrara ((x1 - x2)elevado al cuadradro + (y1 - y2)elevado al cuadradro)
        Double dis = 0.0;
        Double x = co.getUbicacion().getLongitud() - cd.getUbicacion().getLongitud();
        Double y = co.getUbicacion().getLatitud() - cd.getUbicacion().getLatitud();
        //Redondea a 2 decimales
        dis = Math.round(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) * 100.0) / 100.0;
        return dis;
    }

    //funcion que me renueve la matriz de adyacencia con los nuevos datos de la ciudad
    public boolean modificar(Ciudad cv, Ciudad cn) throws Exception {
        //recorro la matriz de adyacencia
        for(int i=1 ; i<= gnde.numVertices();i++){
            //recorro la lista de adyacencia
            for(int j=0 ; j < gnde.getListaAdyacente()[i].getSize();j++){
                //si el vertice es igual a la ciudad que se quiere actualizar
                if(gnde.getListaAdyacente()[i].obtenerDato(j).getDestino().equals(gnde.obtenerCodigo(cv))){
                    //actualizo el peso
                    gnde.getListaAdyacente()[i].obtenerDato(j).setPeso(calcularDistancia(gnde.obtenerEtiqueta(i),cn));
                }
            }
        }
        Boolean modificado = false;
        if(getGnde().modificar(cv , cn))
            modificado=true;
        return modificado;
    }
    
}
