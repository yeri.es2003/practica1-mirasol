package controlador.tda.grafos;

import controlador.tda.grafos.exception.VerticeException;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.exception.PosicionException;

import java.util.List;

/**
 *
 * @author K.G
 */
public class GrafoD extends Grafo {

    protected Integer numV;
    protected Integer numA;
    protected ListaEnlazada<Adyacencia> listaAdyacente[];

    public GrafoD(Integer numV) {
        this.numV = numV;
        this.numA = 0;
        listaAdyacente = new ListaEnlazada[numV + 1];
        for (int i = 0; i <= this.numV; i++) {
            listaAdyacente[i] = new ListaEnlazada<>();
        }
    }

    public ListaEnlazada<Adyacencia>[] getListaAdyacente() {
        return listaAdyacente;
    }

    public void setListaAdyacente(ListaEnlazada<Adyacencia>[] listaAdyacente) {
        this.listaAdyacente = listaAdyacente;
    }

    public void setGrafo(GrafoD grafo){
        this.numV=grafo.numV;
        this.numA=grafo.numA;
        this.listaAdyacente=grafo.listaAdyacente;
    }

    @Override
    public Integer numVertices() {
        return this.numV;
    }

    @Override
    public Integer numAristas() {
        return this.numA;
    }

    /**
     * Permite verificar si existe una conexion entre aristas
     *
     * @param i vertice inicial
     * @param f vertice final
     * @return arreglo de objetos: en la posicion 0 regresa un boolean y en la 1
     * el peso
     * @throws VerticeException
     */
    @Override
    public Object[] existeArista(Integer i, Integer f) throws VerticeException {
        Object[] resultado = {Boolean.FALSE, Double.NaN};
        if (i > 0 && f > 0 && i <= numV && f <= numV) {
            ListaEnlazada<Adyacencia> lista = listaAdyacente[i];
            for (int j = 0; j < lista.getSize(); j++) {
                try {
                    Adyacencia aux = lista.obtenerDato(j);
                    if (aux.getDestino().intValue() == f.intValue()) {
                        resultado[0] = Boolean.TRUE;
                        resultado[1] = aux.getPeso();
                        break;
                    }
                } catch (PosicionException ex) {

                }
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
        return resultado;
    }

    @Override
    public Double pesoArista(Integer i, Integer f) throws VerticeException {
        Double peso = Double.NaN;
        Object[] existe = existeArista(i, f);
        if (((Boolean) existe[0])) {
            peso = (Double) existe[1];
        }
        return peso;
    }

    @Override
    public void insertarArista(Integer i, Integer j) throws VerticeException {
        insertarArista(i, j, Double.NaN);
    }

    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if (i > 0 && j > 0 &&i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
    }
    //metodo que me encuentra el camino mas corto entre dos vertices
    public ListaEnlazada<Integer> algoritmoFloyd (Integer inicio, Integer fin) throws Exception{
        ListaEnlazada<Integer> camino = new ListaEnlazada<>();
        Double [] [] mDistancias = new Double [numV+1] [numV+1];
        Integer [] [] mCaminos = new Integer [numV+1] [numV+1];
        Integer [] [] mCamino = new Integer [numV+1] [numV+1];
        for (int i = 1; i <= numV; i++) {
            for (int j = 1; j <= numV; j++) {
                mCamino[i][j] = j;
                mCaminos[i][j] = i;
                if(i == j){
                    mDistancias[i][j] = 0.0;
                    mCamino[i][j] = 0;
                    mCaminos[i][j] = 0;
                }else if (!(Boolean)existeArista(i,j) [0]) { //Si la listaAdyacencia en la posicion i es vacia, entonces la matriz de adyacencia en la posicion i,j es infinito
                    mDistancias[i][j] = Double.POSITIVE_INFINITY;
                } else { //Si la listaAdyacencia en la posicion i no es vacia, entonces se le asigna el peso de la arista
                    mDistancias[i][j] = pesoArista(i, j);
                }
            }
        }
        for (int k = 1; k <= numV; k++) {
            for (int i = 1; i <= numV; i++) {
                for (int j = 1; j <= numV; j++) {
                    if (mDistancias[i][k] + mDistancias[k][j] < mDistancias[i][j]) {
                        mDistancias[i][j] = mDistancias[i][k] + mDistancias[k][j];
                        mCamino[i][j] = mCaminos[k][j];
                    }
                }
            }
        }
        //me recorre la matriz de caminos para obtener el camino mas corto
        Integer i = inicio;
        Integer j = fin;
        while(i != j){
            camino.insertar(i);
            i = mCamino[i][j];
        }
        camino.insertar(i);
        return camino;
    }
    //metodo que me recorra todos los nodos en busqueda por anchura
    public ListaEnlazada<Integer> BPA(Integer inicio) throws Exception{
        ListaEnlazada<Integer> camino = new ListaEnlazada<>();
        ListaEnlazada<Integer> visitados = new ListaEnlazada<>();
        ListaEnlazada<Integer> pendientes = new ListaEnlazada<>();
        pendientes.insertar(inicio);
        while(!pendientes.estaVacia()){
            Integer actual = pendientes.eliminarDato(0);
            visitados.insertar(actual);
            camino.insertar(actual);
            ListaEnlazada<Adyacencia> lista = listaAdyacente[actual];
            for (int i = 0; i < lista.getSize(); i++) {
                Adyacencia aux = lista.obtenerDato(i);
                if (!visitados.existe(aux.getDestino())){
                    if (!pendientes.existe(aux.getDestino())){
                        pendientes.insertar(aux.getDestino());
                    }
                }
            }
        }
        return camino;
    }

    //metodo que me recorra todos los nodos en busqueda por profundidad
    public ListaEnlazada<Integer> BPP(Integer inicio) throws Exception{
        ListaEnlazada<Integer> camino = new ListaEnlazada<>();
        ListaEnlazada<Integer> visitados = new ListaEnlazada<>();
        ListaEnlazada<Integer> pendientes = new ListaEnlazada<>();
        pendientes.insertarCabecera(inicio);
        while(!pendientes.estaVacia()){
            Integer actual = pendientes.eliminarDato(0);
            visitados.insertar(actual);
            camino.insertar(actual);
            ListaEnlazada<Adyacencia> lista = listaAdyacente[actual];
            for(int i = 0; i < lista.getSize(); i++){
                Adyacencia aux = lista.obtenerDato(i);
                if(!visitados.existe(aux.getDestino())){
                    if(!pendientes.existe(aux.getDestino())){
                        pendientes.insertarCabecera(aux.getDestino());
                    }
                }
            }
        }
        return camino;
    }

    @Override
    public ListaEnlazada<Adyacencia> adyacente(Integer i) throws VerticeException {
        return listaAdyacente[i];
    }

    public void insertarNuevoNodo() {
        GrafoD copia = new GrafoD(this.numVertices()+1);
        try{
            for (int i = 1; i <= this.numVertices(); i++) {
                ListaEnlazada<Adyacencia> lista = this.adyacente(i);
                for (int j = 0; j < lista.getSize(); j++) {
                    Adyacencia aux = lista.obtenerDato(j);
                    copia.insertarArista(i, aux.getDestino(), aux.getPeso());
                }
            }
        }catch (Exception e) {
            System.out.println("Error al copiar el grafo");
        }
        copia.listaAdyacente = new ListaEnlazada[copia.numV + 1];
        for (int i = 0; i <= numV; i++) {
            copia.listaAdyacente[i] = this.listaAdyacente[i];
        }
        copia.listaAdyacente[copia.numV] = new ListaEnlazada<>();
        this.setGrafo(copia);
    }

    public static void main(String[] args) {
        GrafoD grafo = new GrafoD(5);
        
        try {
            grafo.insertarArista(1, 2,4.0);
            grafo.insertarArista(2, 1,4.0);
            grafo.insertarArista(1, 3,8.0);
            grafo.insertarArista(3, 1,8.0);
            grafo.insertarArista(2, 4,2.0);
            grafo.insertarArista(4, 2,2.0);
            grafo.insertarArista(2, 3,1.0);
            grafo.insertarArista(3, 4,4.0);
            grafo.insertarArista(4, 3,4.0);
            grafo.insertarArista(3, 5,2.0);
            grafo.insertarArista(5, 3,2.0);
            grafo.insertarArista(4, 5,7.0);
            grafo.insertarArista(5, 4,7.0);
            ListaEnlazada<Integer> camino = grafo.algoritmoFloyd(3,2);
            camino.imprimir();
            System.out.println("Recorrido por anchura");
            grafo.BPA(2).imprimir();
            System.out.println("Recorrido por profundidad");
            grafo.BPP(2).imprimir();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}