package controlador.tda.grafos;

import controlador.tda.grafos.exception.VerticeException;

/**
 *
 * @author K.G
 */
public class GrafoND extends GrafoD{

    public GrafoND(Integer numV) {
        super(numV);
    }
    
    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if (i > 0 && j > 0 &&i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));
                listaAdyacente[j].insertarCabecera(new Adyacencia(i, peso));
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }

    }
    
    public static void main(String[] args) {
        GrafoND gnd = new GrafoND(5);
        
        try {
            gnd.insertarArista(1, 2,4.0);
            gnd.insertarArista(1, 3,8.0);
            gnd.insertarArista(2, 4,2.0);
            gnd.insertarArista(3, 4,4.0);
            gnd.insertarArista(3, 5,2.0);
            gnd.algoritmoFloyd(3,2);
        } catch (Exception e) {
            System.out.println("ERROR: " +e);
        }
    }
    
}