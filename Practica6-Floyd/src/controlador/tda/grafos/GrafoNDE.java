/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.tda.grafos;

import controlador.tda.grafos.exception.VerticeException;

/**
 *
 * @author LENOVO LEGION 5
 */
public class GrafoNDE <E> extends GrafoDE<E>{
    
    public GrafoNDE(Integer numV, Class clazz) {
        super(numV, clazz);
    }

    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if (i > 0 && j > 0 &&i <= numV && j <= numV) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                numA++;
                listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));
                listaAdyacente[j].insertarCabecera(new Adyacencia(i, peso));
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
    }

    @Override
    public void insertarAristaE(E i, E j, Double peso) throws Exception {
        insertarArista(obtenerCodigo(i), obtenerCodigo(j), peso);
        insertarArista(obtenerCodigo(j), obtenerCodigo(i), peso);
    }

    public static void main(String[] args) {
        GrafoNDE<String> gde = new GrafoNDE<>(5, String.class);
        gde.etiquetarVertice(1, "Karen");
        gde.etiquetarVertice(2, "Adrián");
        gde.etiquetarVertice(3, "Thais");
        gde.etiquetarVertice(4, "Hilary");
        gde.etiquetarVertice(5, "Eda");

        try {
            gde.insertarAristaE("Karen", "Hilary");
            gde.insertarAristaE("Karen", "Eda");
            gde.insertarAristaE("Adrián", "Hilary");
            gde.insertarAristaE("Adrián", "Karen");
            gde.insertarAristaE("Thais", "Karen");
            gde.insertarAristaE("Thais", "Eda");
            gde.insertarAristaE("Hilary", "Eda");
            System.out.println(gde.numVertices());
            gde.insertarNuevoNodo("Juan");
            System.out.println(gde.numVertices());

        } catch (Exception ex) {
        }

        System.out.println(gde.toString());
    }
}
