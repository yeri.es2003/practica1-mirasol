/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package vista;

import controlador.Ciudad.CiudadGrafoController;
import controlador.tda.grafos.Adyacencia;
import controlador.tda.lista.ListaEnlazada;
import controlador.tda.lista.NodoLista;
import modelo.Ciudad;
import modelo.Ubicacion;
import vista.tablas.TablaCiudades;

import javax.swing.JOptionPane;
import java.awt.event.ItemEvent;

/**
 *
 * @author LENOVO LEGION 5
 */
public class FrmCiudad extends javax.swing.JFrame {
    CiudadGrafoController cgc = new CiudadGrafoController();
    TablaCiudades tciudades = new TablaCiudades();
    /**
     * Creates new form FrmCiudad
     */
    public FrmCiudad() {
        try {
            cgc.getGnde().insertarNuevoNodo(new Ciudad(cgc.getGnde().numVertices(), "Balsas", "El Oro", new Ubicacion(4.6, -74.0)));
            cgc.getGnde().insertarNuevoNodo(new Ciudad(cgc.getGnde().numVertices(), "Machala", "El Oro", new Ubicacion(20.6, -14.0)));
            cgc.getGnde().insertarNuevoNodo(new Ciudad(cgc.getGnde().numVertices(), "Marcabeli", "El Oro", new Ubicacion(2.0, -77.0)));
            cgc.getGnde().insertarNuevoNodo(new Ciudad(cgc.getGnde().numVertices(), "Piñas", "El Oro", new Ubicacion(15.0, -90.0)));
            cgc.getGnde().insertarAristaE(cgc.getGnde().obtenerEtiqueta(1), cgc.getGnde().obtenerEtiqueta(2), cgc.calcularDistancia(cgc.getGnde().obtenerEtiqueta(1), cgc.getGnde().obtenerEtiqueta(2)));
            cgc.getGnde().insertarAristaE(cgc.getGnde().obtenerEtiqueta(1), cgc.getGnde().obtenerEtiqueta(3), cgc.calcularDistancia(cgc.getGnde().obtenerEtiqueta(1), cgc.getGnde().obtenerEtiqueta(3)));
            cgc.getGnde().insertarAristaE(cgc.getGnde().obtenerEtiqueta(2), cgc.getGnde().obtenerEtiqueta(4), cgc.calcularDistancia(cgc.getGnde().obtenerEtiqueta(2), cgc.getGnde().obtenerEtiqueta(4)));
            cgc.getGnde().insertarAristaE(cgc.getGnde().obtenerEtiqueta(3), cgc.getGnde().obtenerEtiqueta(4), cgc.calcularDistancia(cgc.getGnde().obtenerEtiqueta(3), cgc.getGnde().obtenerEtiqueta(4)));
            System.out.println(cgc.getGnde().toString());
        }catch (Exception e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }

        initComponents();
        cargarTabla();
        cargarComboVertice();
        this.setSize(795,310);
        this.setResizable(false);
        btnModificar.setEnabled(false);
        this.setLocationRelativeTo(null);
    }

    private void limpiar(){
        txtNombre.setText("");
        txtProvincia.setText("");
        txtLatitud.setText("");
        txtDistancia.setText("");
        txtLongitud.setText("");
        cgc.setCiudad(null);
    }

    private void cargarComboVertice(){
        cbxFinal.removeAllItems();
        cbxInicio.removeAllItems();
        cbxInicio1.removeAllItems();
        for (int i = 1; i <= cgc.getGnde().numVertices(); i++) {
            try{
                String x = cgc.getGnde().obtenerEtiqueta(i).getNombre();
                cbxFinal.addItem(x);
                cbxInicio.addItem(x);
                cbxInicio1.addItem(x);
            }catch (Exception e){
                JOptionPane.showMessageDialog(this, "Error al cargar los vertices");
            }
        }
    }

    public void cargarTabla(){
        System.out.println("Cargando tabla de : " + cgc.getGnde().numVertices());
        tciudades.setGrafo(cgc.getGnde());
        tciudades.fireTableStructureChanged();
        tciudades.fireTableDataChanged();
        tblCiudades.setModel(tciudades);
        tblCiudades.updateUI();
    }

    public void calcularDistancia(){
        int origen = cbxInicio.getSelectedIndex()+1;
        int destino = cbxFinal.getSelectedIndex()+1;
        if (origen == destino) {
            JOptionPane.showMessageDialog(null, "ESCOJA CLIENTES DIFERENTES", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                Double distancia = cgc.calcularDistancia(cgc.getGnde().obtenerEtiqueta(origen), cgc.getGnde().obtenerEtiqueta(destino));
                cgc.getGnde().insertarAristaE(cgc.getGnde().obtenerEtiqueta(origen), cgc.getGnde().obtenerEtiqueta(destino), distancia);
                txtDistancia.setText(distancia.toString());
                System.out.println(cgc.getGnde().toString());
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "No se pudo insertar arista", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public void calcularCaminoFloyd(){
        int origen = cbxInicio.getSelectedIndex()+1;
        int destino = cbxFinal.getSelectedIndex()+1;
        if (origen == destino) {
            JOptionPane.showMessageDialog(null, "ESCOJA CLIENTES DIFERENTES", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            try{
                ListaEnlazada<Ciudad> camino = cgc.getGnde().algoritmoFloyd(cgc.getGnde().obtenerEtiqueta(origen), cgc.getGnde().obtenerEtiqueta(destino));
                String cadena = "";
                for (int i = 0; i < camino.getSize(); i++) {
                    cadena += camino.obtenerDato(i).getNombre() + (i == camino.getSize()-1 ? "" : " => ");
                }
                JOptionPane.showMessageDialog(null, cadena, "CAMINO", JOptionPane.INFORMATION_MESSAGE);
            }catch (Exception e){
                JOptionPane.showMessageDialog(this, "Error al calcular camino");
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtProvincia = new javax.swing.JTextField();
        txtLatitud = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCiudades = new javax.swing.JTable();
        btnCrear = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        txtLongitud = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        btnRPP = new javax.swing.JButton();
        btnRPA = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        cbxInicio1 = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        cbxFinal = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        txtDistancia = new javax.swing.JTextField();
        btnCamino = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        cbxInicio = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Agregar Ciudad"));
        jPanel2.setLayout(null);

        jLabel1.setText("Longitud");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(50, 120, 60, 16);

        jLabel2.setText("Nombre :");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(50, 30, 60, 16);

        jLabel3.setText("Provincia :");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(50, 60, 60, 16);

        jLabel4.setText("Latitud :");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(50, 90, 60, 16);

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        jPanel2.add(txtNombre);
        txtNombre.setBounds(130, 30, 180, 22);

        txtProvincia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtProvinciaActionPerformed(evt);
            }
        });
        jPanel2.add(txtProvincia);
        txtProvincia.setBounds(130, 60, 180, 22);

        txtLatitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLatitudActionPerformed(evt);
            }
        });
        jPanel2.add(txtLatitud);
        txtLatitud.setBounds(130, 90, 180, 22);

        tblCiudades.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblCiudades.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCiudadesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblCiudades);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(480, 20, 240, 130);

        btnCrear.setText("Crear");
        btnCrear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearActionPerformed(evt);
            }
        });
        jPanel2.add(btnCrear);
        btnCrear.setBounds(340, 58, 110, 22);

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        jPanel2.add(btnModificar);
        btnModificar.setBounds(340, 90, 110, 22);

        txtLongitud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLongitudActionPerformed(evt);
            }
        });
        jPanel2.add(txtLongitud);
        txtLongitud.setBounds(130, 120, 180, 22);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 10, 770, 160);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(null);

        btnRPP.setText("Recorrido Por Profundidad");
        btnRPP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRPPActionPerformed(evt);
            }
        });
        jPanel3.add(btnRPP);
        btnRPP.setBounds(520, 10, 210, 22);

        btnRPA.setText("Recorrido Por Anchura");
        btnRPA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRPAActionPerformed(evt);
            }
        });
        jPanel3.add(btnRPA);
        btnRPA.setBounds(270, 10, 210, 22);

        jLabel5.setText("Nodo Inicial");
        jPanel3.add(jLabel5);
        jLabel5.setBounds(40, 10, 70, 16);

        cbxInicio1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxInicio1ItemStateChanged(evt);
            }
        });
        cbxInicio1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbxInicio1MouseClicked(evt);
            }
        });
        jPanel3.add(cbxInicio1);
        cbxInicio1.setBounds(130, 10, 100, 22);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(10, 230, 770, 40);

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(null);

        cbxFinal.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxFinalItemStateChanged(evt);
            }
        });
        cbxFinal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbxFinalMouseClicked(evt);
            }
        });
        jPanel4.add(cbxFinal);
        cbxFinal.setBounds(310, 10, 100, 22);

        jLabel6.setText("Nodo Final");
        jPanel4.add(jLabel6);
        jLabel6.setBounds(230, 10, 70, 16);

        jLabel7.setText("Peso :");
        jPanel4.add(jLabel7);
        jLabel7.setBounds(430, 10, 40, 16);

        jButton2.setText("Vincular");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton2);
        jButton2.setBounds(580, 10, 73, 22);

        txtDistancia.setEnabled(false);
        txtDistancia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistanciaActionPerformed(evt);
            }
        });
        jPanel4.add(txtDistancia);
        txtDistancia.setBounds(470, 10, 70, 22);

        btnCamino.setText("Floyd");
        btnCamino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCaminoActionPerformed(evt);
            }
        });
        jPanel4.add(btnCamino);
        btnCamino.setBounds(670, 10, 70, 22);

        jLabel8.setText("Nodo Inicial");
        jPanel4.add(jLabel8);
        jLabel8.setBounds(30, 10, 70, 16);

        cbxInicio.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxInicioItemStateChanged(evt);
            }
        });
        cbxInicio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbxInicioMouseClicked(evt);
            }
        });
        jPanel4.add(cbxInicio);
        cbxInicio.setBounds(120, 10, 100, 22);

        jPanel1.add(jPanel4);
        jPanel4.setBounds(10, 180, 770, 40);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 790, 420);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtProvinciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtProvinciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtProvinciaActionPerformed

    private void txtLatitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLatitudActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLatitudActionPerformed

    private void btnCrearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearActionPerformed
        // TODO add your handling code here:
        String nombre = txtNombre.getText();
        String provincia = txtProvincia.getText();
        if (nombre.isEmpty() || provincia.isEmpty() || txtLatitud.getText().isEmpty() || txtLongitud.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Debe llenar todos los campos");
        } else {
            try{
                Double longitud1 = Double.parseDouble(txtLongitud.getText());
                Double latitud1 = Double.parseDouble(txtLatitud.getText());
                cgc.getGnde().insertarNuevoNodo(new Ciudad( cgc.getGnde().numVertices(), nombre, provincia, new Ubicacion(longitud1, latitud1)));
                cargarComboVertice();
                limpiar();
                cargarTabla();
            }catch (Exception e) {
                JOptionPane.showMessageDialog(this, "No se pudo insertar el nodo");
            }
        }
    }//GEN-LAST:event_btnCrearActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
        String nombre = txtNombre.getText();
        String provincia = txtProvincia.getText();
        String longitud = txtLongitud.getText();
        String latitud = txtLatitud.getText();
        if (nombre.isEmpty() || provincia.isEmpty() || longitud.isEmpty() || latitud.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Debe llenar todos los campos");
        } else {
            try{
                Double longitud1 = Double.parseDouble(longitud);
                Double latitud1 = Double.parseDouble(latitud);
                int pos = cgc.getGnde().obtenerCodigo(cgc.getCiudad());
                if(cgc.modificar(cgc.getCiudad(), new Ciudad(pos,nombre, provincia, new Ubicacion(longitud1, latitud1)))){
                    JOptionPane.showMessageDialog(this, "Se modifico correctamente");
                    System.out.println(cgc.getGnde().toString());
                    cargarComboVertice();
                    limpiar();
                    cargarTabla();
                    btnModificar.setEnabled(false);
                }else{
                    JOptionPane.showMessageDialog(this, "No se pudo modificar el nodo");
                }
            }catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "No se pudo modificar el nodo");
            }
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void tblCiudadesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCiudadesMouseClicked
        // TODO add your handling code here:
        int fila = tblCiudades.getSelectedRow();
        if(evt.getClickCount() == 2){
            if (fila >= 0) {
                txtNombre.setText(tblCiudades.getValueAt(fila, 1).toString());
                txtProvincia.setText(tblCiudades.getValueAt(fila, 2).toString());
                try{
                    txtLatitud.setText(cgc.getGnde().obtenerEtiqueta(fila+1).getUbicacion().getLatitud().toString());
                    txtLongitud.setText(cgc.getGnde().obtenerEtiqueta(fila+1).getUbicacion().getLongitud().toString());
                }catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "No se pudo obtener la ubicacion");
                }
                btnModificar.setEnabled(true);
            }
            try{
                cgc.setCiudad(cgc.getGnde().obtenerEtiqueta(fila+1));
            }catch (Exception e) {
                JOptionPane.showMessageDialog(this, "No se pudo obtener la ciudad");
            }
            btnModificar.setEnabled(true);
        }
    }//GEN-LAST:event_tblCiudadesMouseClicked

    private void cbxInicioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxInicioItemStateChanged
        //Obtener los items seleccionados calcular la distancia y mostrarla en el label de peso
        
    }//GEN-LAST:event_cbxInicioItemStateChanged

    private void cbxFinalItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxFinalItemStateChanged
        //Obtener los items seleccionados calcular la distancia y mostrarla en el label de peso
        
    }//GEN-LAST:event_cbxFinalItemStateChanged

    private void txtLongitudActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLongitudActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLongitudActionPerformed

    private void cbxFinalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbxFinalMouseClicked
              // TODO add your handling code here:
    }//GEN-LAST:event_cbxFinalMouseClicked

    private void cbxInicioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbxInicioMouseClicked
                // TODO add your handling code here:
    }//GEN-LAST:event_cbxInicioMouseClicked

    private void txtDistanciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistanciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDistanciaActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        calcularDistancia();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnCaminoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCaminoActionPerformed
        calcularCaminoFloyd();// TODO add your handling code here:
    }//GEN-LAST:event_btnCaminoActionPerformed

    private void btnRPAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRPAActionPerformed
        ListaEnlazada<Ciudad> camino = new ListaEnlazada<>();
        int inicio = cbxInicio1.getSelectedIndex()+1;
        //realizar la busqueda por anchura
        try {
            camino = cgc.getGnde().BusquedaPorAnchura(cgc.getGnde().obtenerEtiqueta(inicio));
            String cadena = "";
            for (int i = 0; i < camino.getSize(); i++) {
                cadena += camino.obtenerDato(i).getNombre() + (i == camino.getSize()-1 ? "" : " --- ");
            }
            JOptionPane.showMessageDialog(null, cadena);
        }catch (Exception e) {
            JOptionPane.showMessageDialog(this, "No se pudo realizar la busqueda");
        }
    }//GEN-LAST:event_btnRPAActionPerformed

    private void btnRPPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRPPActionPerformed
        ListaEnlazada<Ciudad> camino = new ListaEnlazada<>();
        int inicio = cbxInicio1.getSelectedIndex()+1;
        //realizar la busqueda por profundidad
        try {
            camino = cgc.getGnde().BusquedaPorProfundidad(cgc.getGnde().obtenerEtiqueta(inicio));
            String cadena = "";
            for (int i = 0; i < camino.getSize(); i++) {
                cadena += camino.obtenerDato(i).getNombre() + (i == camino.getSize()-1 ? "" : " --- ");
            }
            JOptionPane.showMessageDialog(null, cadena );
        }catch (Exception e) {
            JOptionPane.showMessageDialog(this, "No se pudo realizar la busqueda");
        }
    }//GEN-LAST:event_btnRPPActionPerformed

    private void cbxInicio1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxInicio1ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxInicio1ItemStateChanged

    private void cbxInicio1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbxInicio1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxInicio1MouseClicked


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmCiudad().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCamino;
    private javax.swing.JButton btnCrear;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnRPA;
    private javax.swing.JButton btnRPP;
    private javax.swing.JComboBox<String> cbxFinal;
    private javax.swing.JComboBox<String> cbxInicio;
    private javax.swing.JComboBox<String> cbxInicio1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblCiudades;
    private javax.swing.JTextField txtDistancia;
    private javax.swing.JTextField txtLatitud;
    private javax.swing.JTextField txtLongitud;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtProvincia;
    // End of variables declaration//GEN-END:variables
}
