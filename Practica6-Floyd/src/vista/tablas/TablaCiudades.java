package vista.tablas;

import controlador.tda.grafos.GrafoNDE;
import javax.swing.table.AbstractTableModel;
import modelo.Ciudad;

public class TablaCiudades extends AbstractTableModel{
    private GrafoNDE <Ciudad> gnde;

    public GrafoNDE getGrafo() {
        return gnde;
    }

    public void setGrafo(GrafoNDE <Ciudad> grafoD) {
        this.gnde = grafoD;
    }
    
    @Override
    public int getRowCount() {
        return gnde.numVertices();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: return "Nro";
            case 1: return "Nombre";
            case 2: return "Provincia";
            case 3: return "Ubicacion";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        try {
            Ciudad c = gnde.obtenerEtiqueta(arg0+1);
            switch (arg1) {
                case 0: return (arg0+1);
                case 1: return c.getNombre();
                case 2: return c.getProvincia();
                case 3: return (c.getUbicacion() == null)? "No hay" : c.getUbicacion().toString();
                default: return null;
            }
        } catch (Exception ex) {
            System.out.println("Error en la tabla");
            return null;
        }
    }
}
