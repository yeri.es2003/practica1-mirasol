/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import Modelo.Person;
import com.google.gson.Gson;
import controlador.lista.ListaEnlazadaServices;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author LENOVO LEGION 5
 */
public class ControladorPerson {
    public ListaEnlazadaServices<Person> listaPersonas = new ListaEnlazadaServices<Person>();
    
    public ListaEnlazadaServices<Person> getLista(){
        return listaPersonas;
    }
    
    public void guardar() throws IOException{
        Gson json = new Gson();
        String jsons = json.toJson(getLista());
        FileWriter fw = new FileWriter("Person" + ".json");
        fw.write(jsons);
        fw.flush();
    }
   
}
