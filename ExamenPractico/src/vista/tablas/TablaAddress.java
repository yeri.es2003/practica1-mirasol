/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.tablas;
import Modelo.Address;
import controlador.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TablaAddress extends AbstractTableModel {

    private ListaEnlazadaServices<Address> lista = new ListaEnlazadaServices<> ();

    public ListaEnlazadaServices<Address> getlista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Address> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nro ID";
            case 1:
                return "Street";
            case 2:
                return "Suite";
            case 3:
                return "City";
            case 4:
                return "Zipcode";
            default:
                return null;
        }            
    }
    
    @Override
    public Object getValueAt(int arg0,int argl) {
        Address address = lista.obtenerDato(arg0);
        switch (argl) {
            case 0:
                return(arg0 + 1);
            case 1:
                return address.getStreet();
            case 2:
                return address.getSuite();
            case 3:
                return address.getCity();
            case 4:
                return address.getZipcode();
            default:
                return null;
        }
    }
}
