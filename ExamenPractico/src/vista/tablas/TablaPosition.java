/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.tablas;
import Modelo.Position;
import controlador.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TablaPosition extends AbstractTableModel {

    private ListaEnlazadaServices<Position> lista = new ListaEnlazadaServices<> ();

    public ListaEnlazadaServices<Position> getlista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Position> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nro ID";
            case 1:
                return "Latitud";
            case 2:
                return "Longitud";
            default:
                return null;
        }            
    }
    
    @Override
    public Object getValueAt(int arg0,int argl) {
        Position position = lista.obtenerDato(arg0);
        switch (argl) {
            case 0:
                return(arg0 + 1);
            case 1:
                return position.getLat();
            case 2:
                return position.getLng();
            default:
                return null;
        }
    }
}
