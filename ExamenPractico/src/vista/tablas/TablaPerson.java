/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.tablas;

import Modelo.Person;
import controlador.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TablaPerson extends AbstractTableModel {

    private ListaEnlazadaServices<Person> lista = new ListaEnlazadaServices<> ();

    public ListaEnlazadaServices<Person> getlista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Person> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nro ID";
            case 1:
                return "Nombres";
            case 2:
                return "UserName";
            case 3:
                return "Email";
            case 4:
                return "Phone";
            case 5:
                return "WebSite";
            default:
                return null;
        }            
    }
    
    @Override
    public Object getValueAt(int arg0,int argl) {
        Person person = lista.obtenerDato(arg0);
        switch (argl) {
            case 0:
                return(arg0 + 1);
            case 1:
                return person.getName();
            case 2:
                return person.getUsername();
            case 3:
                return person.getEmail();
            case 4:
                return person.getPhone();
            case 5:
                return person.getWebsite();
            default:
                return null;
        }
    }
}