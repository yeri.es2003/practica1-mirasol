/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;

/**
 *
 * @author LENOVO LEGION 5
 */
public class Cliente {
    private String nombres;
    private Integer cedula;
    private LocalDate fecha;
    private Integer tiempo;
    private Integer pago;

    public Cliente(String nombres, Integer cedula, LocalDate fecha, Integer pago) {
        this.nombres = nombres;
        this.cedula = cedula;
        this.fecha = fecha;
        this.pago = pago;
        setTiempo();
    }

    public Cliente() {
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Integer getCedula() {
        return cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public Integer getPago() {
        return pago;
    }

    public void setPago(Integer pago) {
        this.pago = pago;
    }
    
    public void setTiempo(){
        Long x = (Long) DAYS.between(LocalDate.now(), fecha);
        tiempo = x.intValue();
    }
    
    public void setTiempo(Integer x){
        tiempo = x;
    }
    
    public Integer getTiempo(){
        return tiempo;
    } 
    
    public void imprimir(){
        System.out.println(nombres + ", " + cedula + ", " + pago);
    }
    
}
