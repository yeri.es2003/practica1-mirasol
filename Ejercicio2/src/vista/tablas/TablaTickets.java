/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.tablas;

import controlador.cola.ColaServices;
import controlador.exception.PosicionException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import modelo.Ticket;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TablaTickets extends AbstractTableModel{
    ColaServices<Ticket> tc = new ColaServices<>(20);

    public TablaTickets(ColaServices<Ticket> tc) {
        this.tc = tc;
    }
    
    @Override
    public int getRowCount() {
       return tc.getSize();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Hora Atencion";
            case 1:
                return "Asunto";
            case 2:
                return "Nombre";
            case 3:
                return "Cedula";
            default:
                return null;
        }            
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Ticket t =  new Ticket();
        try {
            t = tc.obtenerDato(rowIndex);
        } catch (PosicionException ex) {
            Logger.getLogger(TablaTickets.class.getName()).log(Level.SEVERE, null, ex);
        }
        switch (columnIndex) {
            case 0:
                return t.getHora();
            case 1:
                return t.getAsunto();
            case 2:
                return t.getPersona().getName();
            case 3:
                return t.getPersona().getCedula();
            default:
                return null;
        }
    }
    
}
