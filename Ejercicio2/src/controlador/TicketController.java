/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import controlador.cola.ColaServices;
import modelo.Ticket;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TicketController {
    ColaServices<Ticket> listaTickets = new ColaServices<>(20);

    public TicketController(ColaServices<Ticket> listaTickets) {
        this.listaTickets = listaTickets;
    }

    public TicketController() {
    }

    public ColaServices<Ticket> getListaTickets() {
        return listaTickets;
    }

    public void setListaTickets(ColaServices<Ticket> listaTickets) {
        this.listaTickets = listaTickets;
    }
    
    public int getSize(){
        return listaTickets.getSize();
    }
}
