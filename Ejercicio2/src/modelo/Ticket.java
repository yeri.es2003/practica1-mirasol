/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author LENOVO LEGION 5
 */

enum Asunto{
    APERTURA_CUENTA, TARJETAS, PRESTAMOS;
}

public class Ticket {
    private String hora;
    private Asunto asunto;
    private Person persona;

    public Ticket(String hora, int asunto, Person persona) {
        this.hora = hora;
        switch (asunto) {
            case 0:
                this.asunto = Asunto.APERTURA_CUENTA;
                break;
            case 1:
                this.asunto = Asunto.TARJETAS;
                break;
            case 2:
                this.asunto = Asunto.PRESTAMOS;
                break;
        }
        this.persona = persona;
    }

    public Ticket() {
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Asunto getAsunto() {
        return asunto;
    }

    public void setAsunto(Asunto asunto) {
        this.asunto = asunto;
    }

    public Person getPersona() {
        return persona;
    }

    public void setPersona(Person persona) {
        this.persona = persona;
    }
    
}
