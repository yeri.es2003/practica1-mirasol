/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import com.google.gson.Gson;
import controlador.lista.ListaEnlazadaServices;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;
import modelo.Cliente;

/**
 *
 * @author LENOVO LEGION 5
 */
public class ControladorCliente {

    ListaEnlazadaServices<Cliente> listaCliente = new ListaEnlazadaServices<Cliente>();

    public ListaEnlazadaServices<Cliente> getListaCliente() {
        return listaCliente;
    }

    public void setListaCliente(ListaEnlazadaServices<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }

    public int getSize() {
        return listaCliente.getSize();
    }

    public void guardar() throws IOException {
        Gson json = new Gson();
        Cliente[] clientes = new Cliente[listaCliente.getSize()];
        for (int i = 0; i < listaCliente.getSize(); i++) {
            clientes[i] = listaCliente.obtenerDato(i);
        }
        String jsons = json.toJson(clientes);
        FileWriter fw = new FileWriter("Clientes" + ".json");
        fw.write(jsons);
        fw.flush();
    }

    public void cargar() throws FileNotFoundException, IOException {
        try {
            System.out.println("Cargando data");
            Gson json = new Gson();
            FileReader fr = new FileReader("Clientes" + ".json");
            StringBuilder jsons = new StringBuilder();
            boolean isComa;
            int valor = fr.read();
            while (valor != -1) {
                jsons.append((char) valor);
                valor = fr.read();
            }
            Cliente[] aux = json.fromJson(jsons.toString(), Cliente[].class);
            for (int i = aux.length - 1; i >= 0; i--) {
                aux[i].setTiempo();
                if (aux[i].getTiempo() >= 0) {
                    listaCliente.insertarAlInicio(aux[i]);
                }
            }
        } catch (Exception e) {
            System.out.println("No se encontraron objetos guardados");
        }

    }

    public void llenar(int n) {
        String [] nombres = {"Alex", "Mateo", "Bryan", "Adrian", "Mathias", "Diego", "Joan", "Ainara", "Nicole", "Viviana", "Megan", "Jandry", "Anahi", "Arelis", "Ana", "Paulo", "Alejandro","Felipe", "Daniel", "David", "Jordy", "Juan", "Jose", "Carlos", "Gonzalo","Jean","Andres","Cesar","Christian","Leandro","Leonardo","Stalin","Jefferson","Michael","Elian", "Ariana","Camila","Paul","Ferndando","Ulvio","Ufredo","Mauricio","Abby","Bianca","Clarissa","Maria","Ezequiel","Luis","Pablo","Wilson","Ximena","Sofia","Tomas","Marcos","Jhonson", "John","Hector","Humberto","Alfredo","Steven","Enrique","Cristopher","Santiago","Esteban","Rostin"};
        String [] apellidos = {"Riofrio", "Gonzaga", "Espinoza", "Gallardo", "Rios", "Calva", "Armijos", "Romero", "Valarezo", "Ramirez", "Gonzalez", "Fernandez", "Sanchez", "Aguilar", "Hernandez","Gomez","Diaz","Torres","Apolo","Largo","Castillo","Castro","Diaz","Valle","Largo","Romero","Bravo","Dominguez","Garcia","Cruz","Marquez","Vargas","Murillo","Arroyo","Salazar","Ordoñez","Quishpe","Lopez","Gimenez","Alvarado","Hernandez","Alvarez","Silva","Muñoz","Nuñez","Ortega","Encarnacion","Cortez","Flores","Tapia","Molina","Cueva","Reyes","Navarro","Pachacama","Lince","Ochoa","Atience","Campoverde","Gimenez","Guaman","Espejo","Pogo","Soto","Crespo","Rojas","Lorenzo","Montero","Hidalgo","Mora","Benitez","Vicente","Roman","Cedillo"};
        for (int i = getSize(); i < n; i++) {
            Integer cedula = Integer.valueOf(((int) (Math.random() * 100000000)+ 1100000000));
            String nombre = "";
            int x1 = (int) (Math.random()*(nombres.length));
            int x2 = (int) (Math.random()*(apellidos.length));
            nombre += apellidos [x2] + " " + nombres[x1]  ; 
            int aux = (int) (Math.random() * 4 + 1);
            Integer pago = 0;
            LocalDate fecha = LocalDate.now();
            switch (aux) {
                case 1:
                    pago = 30;
                    fecha = fecha.plusDays(30);
                    break;
                case 2:
                    pago = 55;
                    fecha = fecha.plusDays(60);
                    break;
                case 3:
                    pago = 75;
                    fecha = fecha.plusDays(90);
                    break;
                case 4:
                    pago = 125;
                    fecha = fecha.plusDays(180);
                    break;
            }
            listaCliente.insertarAlFinal(new Cliente(nombre, cedula, fecha, pago));
        }
    }
}
