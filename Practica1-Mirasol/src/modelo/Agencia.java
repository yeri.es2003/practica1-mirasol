/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author LENOVO LEGION 5
 */
public class Agencia {
    private String name = "Default";
    private Object [] [] ventasMes =  new Object [2] [12] ;

    public Agencia(String name) {
        this.name = name;
    }

    public Agencia() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getNombreMes(int pos) {
        return ventasMes [0] [pos];
    }
    
    public Object [] getVentas(){
        return (Object[]) ventasMes [1];
    }
    
    public Object getVentasMes(int pos) {
        return ventasMes [1] [pos];
    }
    
    public void setVentasMes(float venta, int pos, String mes) {
        ventasMes [0] [pos] = mes;
        ventasMes [1] [pos] = venta;
    }
    
}
