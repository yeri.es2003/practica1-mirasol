/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.tabla;

import controlador.AgenciaController;
import javax.swing.table.AbstractTableModel;
import modelo.Agencia;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TablaMayorMenor extends AbstractTableModel {
    private AgenciaController ac = new AgenciaController ();
    private int nMes;

    public TablaMayorMenor( int nMes, AgenciaController ac) {
        this.nMes = nMes;
        this.ac = ac;
    }
    
     @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public int getRowCount() {
        return 2;
    }
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Agencia/Mes";
            case 1:
                return (String) ac.getAgencias(ac.getIndexMayorVentas(nMes)).getNombreMes(nMes);
            case 2:
                return (String) ac.getAgencias(ac.getIndexMenorVentas(nMes)).getNombreMes(nMes);
            
            default:
                return null;
        }            
    }
    
    @Override
    public Object getValueAt(int arg0,int argl) {
        switch (argl) {
            case 0:
                if(arg0 == 0){
                    return ac.getAgencias(ac.getIndexMayorVentas(nMes)).getName() + " (Mayor)";
                }else{
                    return ac.getAgencias(ac.getIndexMenorVentas(nMes)).getName() + " (Menor)";
                }
                
            case 1:
                if(arg0 == 0){
                    return ac.getAgencias(ac.getIndexMayorVentas(nMes)).getVentasMes(nMes);
                }else{
                    return ac.getAgencias(ac.getIndexMenorVentas(nMes)).getVentasMes(nMes);
                }
            default:
                return null;
        }
    }
}
