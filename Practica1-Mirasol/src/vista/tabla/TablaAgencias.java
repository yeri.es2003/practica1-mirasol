/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.tabla;

import controlador.AgenciaController;
import javax.swing.table.AbstractTableModel;
import modelo.Agencia;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TablaAgencias extends AbstractTableModel{
    private AgenciaController ac = new AgenciaController ();
    private int nAgencia;
    private int nMes;
    private int row;
    private int col;

    public TablaAgencias(int row, int col, int nAgencia, int nMes, AgenciaController ac) {
        this.row = row+1;
        this.col = col+2;
        this.ac = ac;
        this.nAgencia = nAgencia;
        this.nMes = nMes;
    }

    @Override
    public int getColumnCount() {
        return col;
    }

    @Override
    public int getRowCount() {
        return row;
    }
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Agencia/Mes";
            case 1:
                if(nMes != 12 && nAgencia !=5){
                    return (String) ac.getAgencias(nAgencia).getNombreMes(nMes);
                }else{
                    return "Enero";
                }
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
            default:
                return null;
        }            
    }
    
    @Override
    public Object getValueAt(int arg0,int argl) {
        Agencia aux;
        if(nAgencia != 5){
            aux = ac.getAgencias(nAgencia);
        }else{
            aux = ac.getAgencias(arg0);
        }
        switch (argl) {
            case 0:
                return aux.getName();
            case 1:
                if(nMes != 12){
                    return aux.getVentasMes(nMes);
                }else{
                    return aux.getVentasMes(0);
                }
            case 2:
                return aux.getVentasMes(1);
            case 3:
                return aux.getVentasMes(2);
            case 4:
                return aux.getVentasMes(3);
            case 5:
                return aux.getVentasMes(4);
            case 6:
                return aux.getVentasMes(5);
            case 7:
                return aux.getVentasMes(6);
            case 8:
                return aux.getVentasMes(7);
            case 9:
                return aux.getVentasMes(8);
            case 10:
                return aux.getVentasMes(9);
            case 11:
                return aux.getVentasMes(10);
            case 12:
                return aux.getVentasMes(11);
            default:
                return null;
        }
    }
}
