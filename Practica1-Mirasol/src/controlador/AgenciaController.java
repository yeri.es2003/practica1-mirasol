/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import modelo.Agencia;

/**
 *
 * @author LENOVO LEGION 5
 */
public class AgenciaController {
    private Agencia [] agencias = new Agencia [5];

    public AgenciaController() {
        agencias [0] = new Agencia("Agencia 1");
        agencias [1] = new Agencia("Agencia 2");
        agencias [2] = new Agencia("Agencia 3");
        agencias [3] = new Agencia("Agencia 4");
        agencias [4] = new Agencia("Agencia 5");
    }

    public Agencia[] getAgencias() {
        return agencias;
    }
    
    public Agencia getAgencias(int pos){
        return agencias[pos];
    }
    
    public void setAgenciaNombre(int x, String name){
        agencias[x].setName(name);
    }

    public void setAgencias(Agencia agencia, int pos) {
        agencias[pos] = agencia;
    }
    
    public void setVentasMes(int ag, float venta, int pos, String mes){
        agencias[ag].setVentasMes(venta, pos, mes);
    }
    
    public float generarPromedioAgencia(int x){
        float promedio = (float) 0.0;
        if(x != 5){
            for (int i = 0; i < agencias[x].getVentas().length; i++) {
                promedio += (agencias[x].getVentasMes(i) == null)? 0.0 : (float) agencias[x].getVentasMes(i);
            } 
            promedio /= agencias[x].getVentas().length;
        }else{
            for (int i = 0; i < agencias.length; i++) {
                float aux = (float) 0.0;
                for (int j = 0; j < agencias[i].getVentas().length; j++) {
                    aux += (agencias[i].getVentasMes(j) == null)? 0.0 : (float) agencias[i].getVentasMes(j);
                }
                aux /= agencias[i].getVentas().length;
                promedio += aux;
            }
            promedio /= agencias.length;
        }
        return promedio;
    }
    
    public float generarPromedioMes(int x){
        float promedio = (float) 0.0;
        if(x != 12){
            for (int i = 0; i < agencias.length; i++) {
                promedio += (agencias[i].getVentasMes(x) == null)? 0.0 : (float) agencias[i].getVentasMes(x);
            } 
            promedio /= agencias.length;
        }else{
            promedio = generarPromedioAgencia(5);
        }
        return promedio;
    }
    
    public int getIndexMayorVentas(int nMes){
        int aux = 0;
        for (int i = 0; i < agencias.length -1; i++) {
            float actual = (float) ((agencias[i].getVentasMes(nMes) == null)? 0.0: (float) agencias[i].getVentasMes(nMes));
            float siguiente = (float) ( (agencias[i+1].getVentasMes(nMes) == null)? 0.0: (float) agencias[i+1].getVentasMes(nMes));
            if(actual < siguiente){
                aux = i+1;
            }
        }
        return aux;
    }
    
    public int getIndexMenorVentas(int nMes){
        int aux = 0;
        for (int i = 0; i < agencias.length -1; i++) {
            float actual = (float) ((agencias[i].getVentasMes(nMes) == null)? 0.0: (float) agencias[i].getVentasMes(nMes));
            float siguiente = (float) ( (agencias[i+1].getVentasMes(nMes) == null)? 0.0: (float) agencias[i+1].getVentasMes(nMes));
            if(actual > siguiente){
                aux = i+1;
            }
        }
        return aux;
    }
    
    public String obtenerMenosVentas(){
        String aux = "El Mes con menos Ventas es : ";
        int x = 0;
        for (int i = 0; i < 11; i++) {
            float sumaMesActual = (generarPromedioMes(i)*5);
            float sumaMesSiguiente = (generarPromedioMes(i+1)*5);
            if(sumaMesActual > sumaMesSiguiente){
                x = i+1;
            }  
        }
        aux += agencias[0].getNombreMes(x) + " con un total de: " + (generarPromedioMes(x)*5) + "Y un promedio de: " + generarPromedioMes(x);
        return aux;
    }
}
