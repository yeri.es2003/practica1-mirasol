/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package vista;

import controlador.MovieController;
import controlador.RatingsController;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Pelicula;
import modelo.Ratings;
import vista.tablas.TablaMovies;
import vista.tablas.TablaRatings;

/**
 *
 * @author LENOVO LEGION 5
 */
public class FrmMain extends javax.swing.JFrame {
    MovieController mvc = new MovieController();
    RatingsController rtc = new RatingsController();
    boolean existenRatings = false;
    int posrts = -1;
    int posmvs = -1;
    boolean editarRatings = false;
    boolean editarMovies = false;
    /**
     * Creates new form FrmMain
     */
    public FrmMain() {
        initComponents();
        try {
            mvc.cargar();
        } catch (Exception e) {
            e.printStackTrace();
        }
        cargarTablaMovies();
        cargarTablaRatings();
    }
    
    public void cargarTablaMovies(){
        TablaMovies tv = new TablaMovies(mvc.getLista());
        tbl_mvs.setModel(tv);
        tbl_mvs.updateUI();
    }
    
    public void cargarTablaRatings(){
        TablaRatings tr = new TablaRatings(rtc.getLista());
        tbl_rts.setModel(tr);
        tbl_rts.updateUI();
    }
    
    public void limpiarRatings(){
        txtSource.setText(null);
        txtValue.setText(null);
        posrts = -1;
        editarRatings = false;
    }
    
    public void limpiarMovies(){
        posmvs = -1;
        editarMovies = false;
        txtTitle.setText(null);
        txtYear.setText(null);
        txtRated.setText(null);
        txtReleased.setText(null);
        txtRuntime.setText(null);
        txtGenre.setText(null);
        txtDirector.setText(null);
        txtWriter.setText(null);
        txtActors.setText(null);
        txtPlot.setText(null);
        txtLanguage.setText(null);
        txtCountry.setText(null);
        txtAwards.setText(null);
        txtPoster.setText(null);
        txtMetascore.setText(null);
        txtimdbRating.setText(null);
        txtimdbVotes.setText(null);
        txtimdbID.setText(null);
        txtType.setText(null);
        txtDVD.setText(null);
        txtBoxOffice.setText(null);
        txtProduction.setText(null);
        txtWebsite.setText(null);
        rdbtnResponse.setSelected(false);
        existenRatings = false;
        editarMovies = false;
    }
    
    public void agregarRatings(){
        if(txtSource.getText().trim().isEmpty() || txtValue.getText().trim().isEmpty()){
            JOptionPane.showMessageDialog(null, "Llene todos los campos ", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            if(editarRatings){
                rtc.getLista().obtenerDato(posrts).setSource(txtSource.getText());
                rtc.getLista().obtenerDato(posrts).setValue(txtValue.getText());
                btnAnadirRts.setText("AÑADIR");
            }else{
                rtc.getLista().insertarAlInicio(new Ratings(txtSource.getText(),txtValue.getText()));
            }
            cargarTablaRatings();
            limpiarRatings();
        }
    }
    
    public void editarRatings(){
        int fila = tbl_rts.getSelectedRow();
        if(fila >= 0){
            txtSource.setText(rtc.getLista().obtenerDato(fila).getSource());
            txtValue.setText(rtc.getLista().obtenerDato(fila).getValue());
            posrts = fila;
            editarRatings = true;
            btnAnadirRts.setText("GUARDAR");
        }else{
            JOptionPane.showMessageDialog(null, "Elige algun registro de la tabla", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminarRatings(){
        int fila = tbl_rts.getSelectedRow();
        if(fila >= 0){
            rtc.getLista().eliminarPosicion(fila);
            cargarTablaRatings();
        }else{
            JOptionPane.showMessageDialog(null, "Elige algun registro de la tabla", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void enviarRatings(){
        if(!existenRatings){
            int pos = (editarMovies == true)? posmvs : 0;
            existenRatings = true;
            if(!editarMovies){//-----------------------------------------------------------------Error se crea una nueva pelicula cada ves que se envian ratings------------------------------
                mvc.getLista().insertarAlInicio(new Pelicula());
            }
            mvc.getLista().obtenerDato(pos).setRatings(rtc.getRatings());
            rtc = new RatingsController();
            cargarTablaRatings();
        }else{
            JOptionPane.showMessageDialog(null, "Ya se han enviado Ratings", "Error", JOptionPane.ERROR_MESSAGE);
            limpiarRatings();
        }
    }
    
    public void editarMovies(){
        int fila = tbl_mvs.getSelectedRow();
        if(fila >= 0){
            txtTitle.setText(mvc.getLista().obtenerDato(fila).getTitle());
            txtYear.setText(mvc.getLista().obtenerDato(fila).getYear());
            txtRated.setText(mvc.getLista().obtenerDato(fila).getRated());
            txtReleased.setText(mvc.getLista().obtenerDato(fila).getReleased());
            txtRuntime.setText(mvc.getLista().obtenerDato(fila).getRuntime());
            txtGenre.setText(mvc.getLista().obtenerDato(fila).getGenre());
            txtDirector.setText(mvc.getLista().obtenerDato(fila).getDirector());
            txtWriter.setText(mvc.getLista().obtenerDato(fila).getWriter());
            txtActors.setText(mvc.getLista().obtenerDato(fila).getActors());
            txtPlot.setText(mvc.getLista().obtenerDato(fila).getPlot());
            txtLanguage.setText(mvc.getLista().obtenerDato(fila).getLanguage());
            txtCountry.setText(mvc.getLista().obtenerDato(fila).getCountry());
            txtAwards.setText(mvc.getLista().obtenerDato(fila).getAwards());
            txtPoster.setText(mvc.getLista().obtenerDato(fila).getPoster());
            txtMetascore.setText(mvc.getLista().obtenerDato(fila).getMetascore());
            txtimdbRating.setText(mvc.getLista().obtenerDato(fila).getImdbRating());
            txtimdbVotes.setText(mvc.getLista().obtenerDato(fila).getImdbVotes());
            txtimdbID.setText(mvc.getLista().obtenerDato(fila).getImdbID());
            txtType.setText(mvc.getLista().obtenerDato(fila).getType());
            txtDVD.setText(mvc.getLista().obtenerDato(fila).getDVD());
            txtBoxOffice.setText(mvc.getLista().obtenerDato(fila).getBoxOffice());
            txtProduction.setText(mvc.getLista().obtenerDato(fila).getProduction());
            txtWebsite.setText(mvc.getLista().obtenerDato(fila).getWebsite());
            rdbtnResponse.setSelected(mvc.getLista().obtenerDato(fila).getResponse());
            rtc.setLista(mvc.getLista().obtenerDato(fila).getRatings());
            posmvs = fila;
            editarMovies = true;
            btnEnviarMvs.setText("GUARDAR");
            cargarTablaRatings();
        }else{
            JOptionPane.showMessageDialog(null, "Elige algun registro de la tabla", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminarMovies(){
        int fila = tbl_mvs.getSelectedRow();
        if(fila >= 0){
            mvc.getLista().eliminarPosicion(fila);
            cargarTablaMovies();
            try {
                mvc.guardar();
            } catch (IOException ex) {
                Logger.getLogger(FrmMain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Elige algun registro de la tabla", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void enviarMovies(){
        if(existenRatings){
            if(txtTitle.getText().trim().isEmpty() || txtYear.getText().trim().isEmpty() || txtRated.getText().trim().isEmpty() || txtReleased.getText().trim().isEmpty() || txtGenre.getText().trim().isEmpty() || txtDirector.getText().trim().isEmpty() ||
               txtMetascore.getText().trim().isEmpty() || txtType.getText().trim().isEmpty() || txtDVD.getText().trim().isEmpty() || txtBoxOffice.getText().trim().isEmpty()){
                JOptionPane.showMessageDialog(null, "Llene todos los campos *Obligatorios*, los campos obligatorios son los que se encuentran de nombre de columna en la tabla", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                int pos = (editarMovies == true)? posmvs : 0;
                mvc.getLista().obtenerDato(pos).setTitle(txtTitle.getText());
                mvc.getLista().obtenerDato(pos).setYear(txtYear.getText());
                mvc.getLista().obtenerDato(pos).setRated(txtRated.getText());
                mvc.getLista().obtenerDato(pos).setReleased(txtReleased.getText());
                mvc.getLista().obtenerDato(pos).setRuntime(txtRuntime.getText());
                mvc.getLista().obtenerDato(pos).setGenre(txtGenre.getText());
                mvc.getLista().obtenerDato(pos).setDirector(txtDirector.getText());
                mvc.getLista().obtenerDato(pos).setWriter(txtWriter.getText());
                mvc.getLista().obtenerDato(pos).setActors(txtActors.getText());
                mvc.getLista().obtenerDato(pos).setPlot(txtPlot.getText());
                mvc.getLista().obtenerDato(pos).setLanguage(txtLanguage.getText());
                mvc.getLista().obtenerDato(pos).setCountry(txtCountry.getText());
                mvc.getLista().obtenerDato(pos).setAwards(txtAwards.getText());
                mvc.getLista().obtenerDato(pos).setPoster(txtPoster.getText());
                mvc.getLista().obtenerDato(pos).setMetascore(txtMetascore.getText());
                mvc.getLista().obtenerDato(pos).setImdbRating(txtimdbRating.getText());
                mvc.getLista().obtenerDato(pos).setImdbVotes(txtimdbVotes.getText());
                mvc.getLista().obtenerDato(pos).setImdbID(txtimdbID.getText());
                mvc.getLista().obtenerDato(pos).setType(txtType.getText());
                mvc.getLista().obtenerDato(pos).setDVD(txtDVD.getText());
                mvc.getLista().obtenerDato(pos).setBoxOffice(txtBoxOffice.getText());
                mvc.getLista().obtenerDato(pos).setProduction(txtProduction.getText());
                mvc.getLista().obtenerDato(pos).setWebsite(txtWebsite.getText());
                mvc.getLista().obtenerDato(pos).setResponse(rdbtnResponse.isSelected());
                cargarTablaMovies();
                btnEnviarMvs.setText("Enviar");
                limpiarMovies();
            }
        }else{
            JOptionPane.showMessageDialog(null, "No se han enviado Ratings, agregue algunos y envialos desde la seccion de Rating Managment", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void guardarJson() throws IOException{
        mvc.guardar();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtTitle = new javax.swing.JTextField();
        txtYear = new javax.swing.JTextField();
        txtRated = new javax.swing.JTextField();
        txtReleased = new javax.swing.JTextField();
        txtRuntime = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtDirector = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtWriter = new javax.swing.JTextField();
        txtActors = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtPlot = new javax.swing.JTextField();
        txtLanguage = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtAwards = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtPoster = new javax.swing.JTextField();
        txtMetascore = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtimdbRating = new javax.swing.JTextField();
        txtimdbVotes = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtType = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtDVD = new javax.swing.JTextField();
        txtBoxOffice = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txtProduction = new javax.swing.JTextField();
        txtWebsite = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtGenre = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtCountry = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txtimdbID = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_mvs = new javax.swing.JTable();
        btnEditarMvs = new javax.swing.JButton();
        btnEnviarMvs = new javax.swing.JButton();
        btnEliminarMvs = new javax.swing.JButton();
        rdbtnResponse = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        txtValue = new javax.swing.JTextField();
        txtSource = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        btnAnadirRts = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_rts = new javax.swing.JTable();
        btnEditarRts = new javax.swing.JButton();
        btnEnviarRts = new javax.swing.JButton();
        btnEliminarRts = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Movie Managment"));

        jLabel1.setText("Title");

        jLabel2.setText("Year");

        jLabel3.setText("Rated");

        jLabel4.setText("Released");

        jLabel5.setText("Runtime");

        jLabel6.setText("Director");

        jLabel7.setText("Writer");

        jLabel8.setText("Actors");

        jLabel9.setText("Plot");

        jLabel10.setText("Language");

        jLabel11.setText("Awards");

        jLabel12.setText("Poster");

        jLabel13.setText("Metascore");

        jLabel14.setText("imdbRating");

        jLabel15.setText("imdbVotes");

        jLabel16.setText("Type");

        jLabel17.setText("DVD");

        jLabel18.setText("BoxOffice");

        jLabel19.setText("Production");

        jLabel20.setText("Website");

        jLabel21.setText("Genre");

        jLabel22.setText("Country");

        jLabel23.setText("imdbID");

        tbl_mvs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbl_mvs);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnEditarMvs.setText("EDITAR");
        btnEditarMvs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarMvsActionPerformed(evt);
            }
        });

        btnEnviarMvs.setText("ENVIAR");
        btnEnviarMvs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarMvsActionPerformed(evt);
            }
        });

        btnEliminarMvs.setText("ELIMINAR");
        btnEliminarMvs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarMvsActionPerformed(evt);
            }
        });

        rdbtnResponse.setText("Response");
        rdbtnResponse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbtnResponseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel1))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel2))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtRated, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel3))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtReleased, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel4))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtRuntime, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel5)))
                                .addGap(26, 26, 26)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(157, 157, 157)
                                        .addComponent(jLabel6))
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                            .addGap(4, 4, 4)
                                            .addComponent(txtLanguage, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jLabel10)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(btnEliminarMvs))
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanel2Layout.createSequentialGroup()
                                                    .addComponent(txtPlot, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(jLabel9))
                                                .addGroup(jPanel2Layout.createSequentialGroup()
                                                    .addComponent(txtWriter, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(jLabel7))
                                                .addGroup(jPanel2Layout.createSequentialGroup()
                                                    .addComponent(txtActors, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(jLabel8))
                                                .addComponent(txtDirector, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGap(18, 18, 18)
                                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(btnEditarMvs, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                                                .addComponent(btnEnviarMvs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtGenre, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel21)
                                .addGap(51, 51, 51)
                                .addComponent(txtCountry, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel22)))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtAwards, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel11))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtPoster, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel12))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtMetascore, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel13))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtimdbRating, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel14))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtimdbVotes, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel15))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtimdbID, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel23)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rdbtnResponse)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtType, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel16))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtDVD, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel17))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtBoxOffice, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel18))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtProduction, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel19))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtWebsite, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel20)))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtAwards, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPoster, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtMetascore, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtimdbRating, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtimdbVotes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDirector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtWriter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtActors, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8)
                                    .addComponent(btnEditarMvs))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtPlot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9)))
                            .addComponent(btnEnviarMvs))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtLanguage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)
                            .addComponent(btnEliminarMvs)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtRated, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtReleased, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtRuntime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDVD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtBoxOffice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtProduction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtWebsite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtimdbID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel23))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtGenre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel21)
                        .addComponent(txtCountry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel22))
                    .addComponent(rdbtnResponse))
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Ratings Managment"));

        jLabel25.setText("Source");

        jLabel26.setText("Value");

        btnAnadirRts.setText("AÑADIR");
        btnAnadirRts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnadirRtsActionPerformed(evt);
            }
        });

        tbl_rts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_rts);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(132, 132, 132))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnEditarRts.setText("EDITAR");
        btnEditarRts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarRtsActionPerformed(evt);
            }
        });

        btnEnviarRts.setText("ENVIAR");
        btnEnviarRts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarRtsActionPerformed(evt);
            }
        });

        btnEliminarRts.setText("ELIMINAR");
        btnEliminarRts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarRtsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtSource, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtValue, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel25)
                                    .addComponent(jLabel26)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(53, 53, 53)
                                .addComponent(btnAnadirRts)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(btnEditarRts)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnEliminarRts)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnEnviarRts))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26))
                .addGap(18, 18, 18)
                .addComponent(btnAnadirRts)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditarRts)
                    .addComponent(btnEnviarRts)
                    .addComponent(btnEliminarRts))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jButton3.setText("GUARDAR / GENERAR JSON");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAnadirRtsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnadirRtsActionPerformed
        agregarRatings();        // TODO add your handling code here:
    }//GEN-LAST:event_btnAnadirRtsActionPerformed

    private void btnEditarRtsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarRtsActionPerformed
        editarRatings();        // TODO add your handling code here:
    }//GEN-LAST:event_btnEditarRtsActionPerformed

    private void btnEnviarRtsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarRtsActionPerformed
        enviarRatings();        // TODO add your handling code here:
    }//GEN-LAST:event_btnEnviarRtsActionPerformed

    private void btnEliminarRtsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarRtsActionPerformed
        eliminarRatings();        // TODO add your handling code here:
    }//GEN-LAST:event_btnEliminarRtsActionPerformed

    private void btnEliminarMvsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarMvsActionPerformed
        eliminarMovies();        // TODO add your handling code here:
    }//GEN-LAST:event_btnEliminarMvsActionPerformed

    private void btnEditarMvsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarMvsActionPerformed
        editarMovies();        // TODO add your handling code here:
    }//GEN-LAST:event_btnEditarMvsActionPerformed

    private void btnEnviarMvsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarMvsActionPerformed
        enviarMovies();        // TODO add your handling code here:
    }//GEN-LAST:event_btnEnviarMvsActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            guardarJson();        // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(FrmMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void rdbtnResponseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbtnResponseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdbtnResponseActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmMain().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnadirRts;
    private javax.swing.JButton btnEditarMvs;
    private javax.swing.JButton btnEditarRts;
    private javax.swing.JButton btnEliminarMvs;
    private javax.swing.JButton btnEliminarRts;
    private javax.swing.JButton btnEnviarMvs;
    private javax.swing.JButton btnEnviarRts;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JRadioButton rdbtnResponse;
    private javax.swing.JTable tbl_mvs;
    private javax.swing.JTable tbl_rts;
    private javax.swing.JTextField txtActors;
    private javax.swing.JTextField txtAwards;
    private javax.swing.JTextField txtBoxOffice;
    private javax.swing.JTextField txtCountry;
    private javax.swing.JTextField txtDVD;
    private javax.swing.JTextField txtDirector;
    private javax.swing.JTextField txtGenre;
    private javax.swing.JTextField txtLanguage;
    private javax.swing.JTextField txtMetascore;
    private javax.swing.JTextField txtPlot;
    private javax.swing.JTextField txtPoster;
    private javax.swing.JTextField txtProduction;
    private javax.swing.JTextField txtRated;
    private javax.swing.JTextField txtReleased;
    private javax.swing.JTextField txtRuntime;
    private javax.swing.JTextField txtSource;
    private javax.swing.JTextField txtTitle;
    private javax.swing.JTextField txtType;
    private javax.swing.JTextField txtValue;
    private javax.swing.JTextField txtWebsite;
    private javax.swing.JTextField txtWriter;
    private javax.swing.JTextField txtYear;
    private javax.swing.JTextField txtimdbID;
    private javax.swing.JTextField txtimdbRating;
    private javax.swing.JTextField txtimdbVotes;
    // End of variables declaration//GEN-END:variables
}
