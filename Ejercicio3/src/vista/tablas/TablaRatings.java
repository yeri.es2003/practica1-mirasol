/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.tablas;

import controlador.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;
import modelo.Ratings;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TablaRatings extends AbstractTableModel{
    ListaEnlazadaServices<Ratings> rtc = new ListaEnlazadaServices<>();

    public TablaRatings(ListaEnlazadaServices<Ratings> rtc) {
        this.rtc = rtc;
    }
    
    @Override
    public int getRowCount() {
       return rtc.getSize();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Source";
            case 1:
                return "Value";
            default:
                return null;
        }            
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Ratings r = rtc.obtenerDato(rowIndex);
        switch (columnIndex) {
            case 0:
                return r.getSource();
            case 1:
                return r.getValue();
            default:
                return null;
        }
    }
    
}
