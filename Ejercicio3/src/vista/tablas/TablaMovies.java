/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.tablas;

import controlador.lista.ListaEnlazadaServices;
import javax.swing.table.AbstractTableModel;
import modelo.Pelicula;

/**
 *
 * @author LENOVO LEGION 5
 */
public class TablaMovies extends AbstractTableModel{
    ListaEnlazadaServices<Pelicula> mvc = new ListaEnlazadaServices<>();

    public TablaMovies(ListaEnlazadaServices<Pelicula> mvc) {
        this.mvc = mvc;
    }
    
    @Override
    public int getRowCount() {
       return mvc.getSize();
    }

    @Override
    public int getColumnCount() {
        return 12;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Title";
            case 1:
                return "Year";
            case 2:
                return "Rated";
            case 3:
                return "Released";
            case 4:
                return "Genre";
            case 5:
                return "Director";
            case 6:
                return "MetaScore";
            case 7:
                return "Type";
            case 8:
                return "DVD";
            case 9:
                return "BoxOffice";
            case 10:
                return "Response";
            case 11:
                return "Ratings size";
            default:
                return null;
        }            
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Pelicula p = mvc.obtenerDato(rowIndex);
        switch (columnIndex) {
            case 0:
                return p.getTitle();
            case 1:
                return p.getYear();
            case 2:
                return p.getRated();
            case 3:
                return p.getReleased();
            case 4:
                return p.getGenre();
            case 5:
                return p.getDirector();
            case 6:
                return p.getMetascore();
            case 7:
                return p.getType();
            case 8:
                return p.getDVD();
            case 9:
                return p.getBoxOffice();
            case 10:
                return p.getResponse();
            case 11:
                return p.getRatings().length;
            default:
                return null;
        }
    }
    
}
