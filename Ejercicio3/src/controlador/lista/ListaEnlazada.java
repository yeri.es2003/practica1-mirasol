/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.lista;


/**
 *
 * @author LENOVO LEGION 5
 */
public class ListaEnlazada <E>{
    private NodoLista <E> cabecera;  
    private Integer size;
    
    /*
    * Contructor de la clase se inicializa en null la cabecera y en 0 el size;
    */
    
    public ListaEnlazada() {
        this.cabecera = null;
        size = 0;
    }
    
    /*
    * Permite ver si la lista esta vacia 
    * @return true si esta vacia, false si esta llena
    */
    
    public Boolean estaVacia(){
        return cabecera == null;
    }

    public Integer getSize() {
        return size;
    }
    
    public void insertar(E dato){
        NodoLista <E> nuevo = new NodoLista(dato, null);
        if(estaVacia()){
            cabecera = nuevo;
        }else{
            NodoLista <E> aux = cabecera;
            while(aux.getSiguiente() != null){
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(nuevo);
        }
        size++;
    }
    
    public void insertar(E dato, int pos) throws PosicionException{
        if(estaVacia() || pos == size){
            insertar(dato);
        }else if(pos >=0 && pos <= size){
            NodoLista <E> nuevo = new NodoLista(dato, null);
            if(pos == 0){
                nuevo.setSiguiente(cabecera);
                cabecera = nuevo;
                size++;
            }else{
                NodoLista <E> aux = cabecera;
                for(int i = 0; i < pos - 1; i++){
                    aux = aux.getSiguiente();
                }
                NodoLista <E> siguiente = aux.getSiguiente();
                nuevo.setSiguiente(siguiente);
                aux.setSiguiente(nuevo);
                size++;
            }
        } else{
            throw new PosicionException("No existe la posicion dada");
        }
    }
    
    public void eliminar(int pos) throws PosicionException{
        if(estaVacia()){
            throw new PosicionException("La lista esta vacia por ende no hay datos para eliminar"); 
        }else{
            if(pos >=0 && pos <= size){
                if(pos == 0){
                    cabecera = cabecera.getSiguiente();
                }else{
                    NodoLista <E> aux = cabecera;
                    for (int i = 0; i < size-1; i++) {
                        if(i >= pos){
                            aux.setDato(aux.getSiguiente().getDato());
                        }
                        aux = aux.getSiguiente();
                    }
                }
                size--;
            } else{
                throw new PosicionException("No existe la posicion dada");
            }
        }
    }
    
    public E buscar(int pos) throws PosicionException{
        if(!estaVacia()){
            if(pos >=0 && pos <= size){
                if(pos == 0){
                    return cabecera.getDato();
                }else{
                    NodoLista <E> aux = cabecera;
                    for(int i = 0; i < pos; i++){
                        aux = aux.getSiguiente();
                    }
                    return aux.getDato();
                }
            }else{
                throw new PosicionException("No existe la posicion dada");
            }
        }else{
           throw new PosicionException("La lista esta vacia por ende no hay posicion"); 
        }
    }
    
    public boolean existeEnLista(E dato) throws PosicionException{
        if(!estaVacia()){
            NodoLista <E> aux = cabecera;
            for(int i = 0; i < size; i++){
                if(aux.getDato() == dato){
                    return true;
                }
            aux = aux.getSiguiente();
            }
            return false;
        }else{
           throw new PosicionException("La lista esta vacia por ende no Datos"); 
        }
    }
    
    public void modificar(int pos, E dato){
        if(estaVacia()){
            if(pos >=0 && pos <= size){
                if(pos == 0){
                    cabecera.setDato(dato);
                }else{
                    NodoLista <E> aux = cabecera;
                    for(int i = 0; i < pos; i++){
                        aux = aux.getSiguiente();
                    }
                    aux.setDato(dato);
                }
            }
        }
    }
    
    public void vaciar(){
        cabecera = null;
        size = 0;
    }
    
    public void imprimir(){
        NodoLista <E> aux = cabecera;
        for (int i = 0; i < size; i++) {
            System.out.println(aux.getDato());
            aux = (aux.getSiguiente());
        }
    }
    
}
