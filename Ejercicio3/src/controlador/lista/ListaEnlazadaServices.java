/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.lista;


import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author LENOVO LEGION 5
 */
@XmlRootElement
public class ListaEnlazadaServices <E>{
    private ListaEnlazada<E> lista = new ListaEnlazada<>();
    
    public ListaEnlazadaServices(){
        this.lista = new ListaEnlazada<>();
    }
    
    public boolean insertarAlFinal(E dato){
        try{
            lista.insertar(dato , lista.getSize());
            return true;
        }catch(PosicionException e){
            System.out.println(e);
        }
        return false;
    }
    
    public boolean insertarAlInicio(E dato){
        try{
            lista.insertar(dato , 0);
            return true;
        }catch(PosicionException e){
            System.out.println(e);
        }
        return false;
    }
    
    public boolean insertar(E dato, int pos){
        try{
            lista.insertar(dato , pos);
            return true;
        }catch(PosicionException e){
            System.out.println(e);
        }
        return false;
    }
    
    public int getSize(){
        return lista.getSize();
    }
    
    public E obtenerDato(Integer pos){
        try {
            return lista.buscar(pos);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        } 
    }
    
    public boolean eliminarCabecera (){
        try {
            lista.eliminar(getSize()-1);
            return true;
        } catch (PosicionException e) {
            System.out.println(e);
        }
        return false;
    }
    
    public boolean eliminarUltimo(){
        try {
            lista.eliminar(lista.getSize()-1);
            return true;
        } catch (Exception e) {
        }
        return false;
    }
    
    public boolean eliminarPosicion(Integer pos){
        try {
            lista.eliminar(pos);
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
    
    public Boolean modificarDato(E dato, Integer pos){
        try{
            lista.modificar(pos, dato);
        return true;
        }catch (Exception e){
            System.out.println(e);
        }
        return false;
    }
    
    public void imprimir(){
        lista.imprimir();
    }
}
