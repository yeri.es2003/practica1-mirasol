/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import controlador.lista.ListaEnlazadaServices;
import modelo.Ratings;

/**
 *
 * @author LENOVO LEGION 5
 */
public class RatingsController {
    ListaEnlazadaServices<Ratings> listaRatings =  new ListaEnlazadaServices<>();

    public ListaEnlazadaServices<Ratings> getLista() {
        return listaRatings;
    }
    
    public Ratings [] getRatings(){
        Ratings [] aux = new Ratings [listaRatings.getSize()];
        for (int i = 0, j = aux.length-1; i < aux.length; i++, j--) {
            aux [i] = listaRatings.obtenerDato(j);
        }
        return aux;
    }

    public void setLista(Ratings [] ratings) {
        listaRatings =  new ListaEnlazadaServices<Ratings>();
        for (int i = 0; i < ratings.length; i++) {
            listaRatings.insertarAlInicio(ratings [i]);
        }
    }
    
    public int getSize(){
        return listaRatings.getSize();
    }
}
