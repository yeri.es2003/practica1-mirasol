/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import com.google.gson.Gson;
import controlador.lista.ListaEnlazada;
import controlador.lista.ListaEnlazadaServices;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import modelo.Pelicula;

/**
 *
 * @author LENOVO LEGION 5
 */
public class MovieController {
    ListaEnlazadaServices<Pelicula> listaPelicula = new ListaEnlazadaServices<>();

    public ListaEnlazadaServices<Pelicula> getLista() {
        return listaPelicula;
    }

    public void setLista(ListaEnlazadaServices<Pelicula> listaPelicula) {
        this.listaPelicula = listaPelicula;
    }
    
    public int getSize(){
        return listaPelicula.getSize();
    }
    
    public void guardar() throws IOException{
        Gson json = new Gson();
        Pelicula [] peliculas = new Pelicula [listaPelicula.getSize()];
        for (int i = 0; i < listaPelicula.getSize(); i++) {
            peliculas [i] = listaPelicula.obtenerDato(i);
        }
        String jsons = json.toJson(peliculas);
        FileWriter fw = new FileWriter("Person" + ".json");
        fw.write(jsons);
        fw.flush();
    }
    
    public void cargar() throws FileNotFoundException, IOException{ 
        Gson json = new Gson();
        FileReader fr = new FileReader("Person" + ".json");
        String jsons = "";
        int valor = fr.read();
        while (valor!=-1) {            
            jsons += String.valueOf((char) valor);
            valor = fr.read();
        }
        System.out.println(jsons);
        Pelicula [] aux = json.fromJson(jsons, Pelicula[].class);
        for (int i = aux.length-1; i >= 0; i--) {
            listaPelicula.insertarAlInicio(aux [i]);
        }
    }
}
